var gulp = require('gulp'),
	exec = require('child_process').exec,
    livereload = require('gulp-livereload');

gulp.task('dev', function () {
    exec('locally -p 9000');
    livereload.listen();

    gulp.watch('**/*.css', function (file) {
        livereload.changed(file);
    });

    gulp.watch('**/*.js', function (file) {
        livereload.changed(file);
    });

    gulp.watch('**/*.html', function (file) {
        livereload.changed(file);
    });
});
